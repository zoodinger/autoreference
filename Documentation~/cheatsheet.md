﻿# Auto-Reference Cheat Sheet

This document provides a very quick overview of the available functionality. Not all types, attributes or settings
are included here. Refer to the [documentation](./index.md) for more comprehensive information.

## Primary Attributes

Add *one* of these to a serializable field:

- `Get`
- `GetInChildren`
- `GetInParent`
- `GetInSiblings`
- `FindInScene`
- `FindInAssets`
- `FindInParent`

## Filters

Add one more of these to fine-tune the results of the primary attribute:

- `ExactType`
- `TypeConstraint`
- `Contains`
- `ContainsInChildren`
- `ContainsInParent`


- `IgnoreSelf`
- `IgnoreInactive`
- `IgnoreDisabled`


- `Layer`
- `Tag`
- `Name`
- `FilterBy`


- `IgnoreNested`


- `Unique`


- `SortByID`
- `SortByDistance`
- `SortByName`
- `Sort`
- `SortBy`


- `Reverse`


- `Take`
- `TakeLast`
- `TakeWhile`

**Note**: Filters are *not* applied in the order they're given in the code, due to how C# works. They're applied
roughly in the order given above.

Please refer to [this section](./index.md#filter-priority-order) in the documentation for more information.

## Useful utilities

- Various sync operations: `Tools`>`Auto-Reference`
- Open Auto-Reference Window: `Tools`>`Auto-Reference`>`Auto-ReferenceWindow`
- Access `Project Settings`>`Auto-Reference` to control automatic syncing.
- Access `Preferences`>`Auto-Reference` to control appearance and logging settings.

## Other Useful Types

- `AutoReferenceEditor`: Inherit this for custom editors
- `OnAfterSync`: Provide a method name for an after-sync callback

## Useful methods

- `AutoReference.Sync(MonoBehaviour or GameObject)`: Sync on demand (does not work during playing)
- `AutoReference.HasSyncInformation(MonoBehaviour or Type)`: Get if type has any auto-reference sync information.
- `AutoReference.SetDirty(Object)`: Same as `EditorUtility.SetDirty(Object)` but doesn't cause errors when building
  in non-Editor code.

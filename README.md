﻿# Auto-Reference Toolkit

The Auto-Reference toolkit is a helpful tool for Unity game development, designed to streamline the process of getting,
validating, and setting references to assets or components in scenes during editor time.

[View documentation](https://zoodinger.gitlab.io/autoreference/index.html)

[Quick reference cheat sheet](https://zoodinger.gitlab.io/autoreference/cheatsheet.html)

## Installation Process

Install this is a package through the Package Manager.

1. Open the Package Manager window by going through `Window > Package Manager`
2. Click the plus sign on the top left, and select `Add package from git URL...`
3. Paste the git url of the repository ('https://gitlab.com/zoodinger/autoreference.git')
4. Press `Add`
